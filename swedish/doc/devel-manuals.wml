#use wml::debian::ddp title="Debians Utvecklarmanualer"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/devel-manuals.defs"
#use wml::debian::translation-check translation="78f856f0bff4125a68adb590b66952d97f183227"

<document "Debians policyhandbok" "policy">

<div class="centerblock">
<p>
  Denna handbok beskriver policykraven på Debian GNU/Linuxdistributionen.
  Detta inbegriper strukturen på och innehållet i Debianarkivet, flera
  formgivningsfrågor gällande operativsystemet, såväl som tekniska krav som
  alla paket måste uppfylla för att inkluderas i distributionen.

<doctable>
  <authors "Ian Jackson, Christian Schwarz, David A. Morris">
  <maintainer "The Debian Policy group">
  <status>
  klar
  </status>
  <availability>
  <inpackage "debian-policy">
  <inddpvcs-debian-policy>
  <p><a href="https://bugs.debian.org/debian-policy">Föreslagna tillägg</a> till Policyn</p>

  <p>Ytterligare policydokumentation:</p>
  <ul>
    <li><a href="packaging-manuals/fhs/fhs-3.0.html">Filesystem Hierarchy Standard</a>
    [<a href="packaging-manuals/fhs/fhs-3.0.pdf">PDF</a>]
    [<a href="packaging-manuals/fhs/fhs-3.0.txt">ren text</a>]
    <li><a href="debian-policy/upgrading-checklist.html">Upgraderingschecklista</a>
    <li><a href="packaging-manuals/virtual-package-names-list.yaml">Förteckning över namn på virtuella paket</a>
    <li><a href="packaging-manuals/menu-policy/">Menypolicy</a>
    [<a href="packaging-manuals/menu-policy/menu-policy.txt.gz">ren text</a>]
    <li><a href="packaging-manuals/perl-policy/">Perlpolicy</a>
    [<a href="packaging-manuals/perl-policy/perl-policy.txt.gz">ren text</a>]
    <li><a href="packaging-manuals/debconf_specification.html">debconfspecification</a>
    <li><a href="packaging-manuals/debian-emacs-policy">Emacspolicy</a>
    <li><a href="packaging-manuals/java-policy/">Javapolicy</a>
    <li><a href="packaging-manuals/python-policy/">Pythonpolicy</a>
	 <li><a href="packaging-manuals/copyright-format/1.0/">Specifikation för Copyright-format</a>
  </ul>
  </availability>
</doctable>
</div>

<hr>

<document "Debians utvecklarreferens" "devref">

<div class="centerblock">
<p>
  Denna handbok beskriver procedurer och resurser för Debianutvecklare.
  Den beskriver hur man blir ny utvecklare, insändningsprocedurer,
  hur felrapporteringssystemet hanteras, sändlistorna, Internetservrar,
  osv.

  <p>Denna handbok är tänkt som en <em>referenshandbok</em> för alla
  Debianutvecklare (nya som gamla).

<doctable>
  <authors "Ian Jackson, Christian Schwarz, Lucas Nussbaum, Rapha&euml;l Hertzog, Adam Di Carlo, Andreas Barth">
  <maintainer "Lucas Nussbaum, Hideki Yamane, Holger Levsen">
  <status>
  klar
  </status>
  <availability>
  <inpackage "developers-reference">
  <inddpvcs-developers-reference>
  </availability>
</doctable>
</div>

<hr>

<document "Guide för Debian Maintainers" "debmake-doc">

<div class="centerblock">
<p>
Detta tutorialdokument beskriver skapande av Debianpaket för vanliga
Debiananvändare och framtida utvecklare med hjälp av kommandot <code>debmake</code>.
</p>
<p>
Den fokuserar på modern paketeringsstil och kommer med många enkla exempel.
</p>
<ul>
<li>POSIX skalskriptpaketering</li>
<li>Python3 skriptpaketering</li>
<li>C med Makefile/Autotools/CMake</li>
<li>flera binära paket med delat bibliotek osv.</li>
</ul>
<p>
Denna “Guide för Debian Maintainers” kan anses vara efterföljaren till
“Debian New Maintainers’ Guide”.
</p>

<doctable>
  <authors "Osamu Aoki">
  <maintainer "Osamu Aoki">
  <status>
  klar
  </status>
  <availability>
  <inpackage "debmake-doc">
  <inddpvcs-debmake-doc>
  </availability>
</doctable>
</div>

<hr>


<document "Debian New Maintainers' Guide" "maint-guide">

<div class="centerblock">
<p>
  Detta dokument kommer att försöka beskriva hur man skapar Debian
  GNU/Linux-paket för vanliga Debiananvändare (och blivande utvecklare),
  med vanligt språk, och med flera fungerande exempel.

  <p>Till skillnad från tidigare försök baseras denna på
  <code>debhelper</code> och de nya verktygen som är tillgängliga för
  utvecklarna.
  Författaren försöker ta med och sammanföra tidigare insatser.

<doctable>
  <authors "Josip Rodin, Osamu Aoki">
  <maintainer "Osamu Aoki">
  <status>
  klar
  </status>
  <availability>
  <inpackage "maint-guide">
  <inddpvcs-maint-guide>
  </availability>
</doctable>
</div>

<hr>


<document "Introduktion till Debianpaketering" "packaging-tutorial">

<div class="centerblock">
<p>
Denna handbok är en introduktion till Debianpaketering.
Den lär potentiella utvecklare hur man modifierar existerande paket, hur man
skapar egna paket, och hur man interagerar med Debiangemenskapen.
Utöver huvudhandledningen så inkluderar den tre praktiska övningar med ämnena
modifikation av <code>grep</code>-paketet samt paketering av spelet
<code>gnujump</code> och ett Javabibliotek.
</p>

<doctable>
  <authors "Lucas Nussbaum">
  <maintainer "Lucas Nussbaum">
  <status>
  klar
  </status>
  <availability>
  <inpackage "packaging-tutorial">
  <inddpvcs-packaging-tutorial>
  </availability>
</doctable>
</div>

<hr>


<document "Debians menysystem" "menu">

<div class="centerblock">
<p>
  Denna manual beskriver Debians menysystem och
  <strong>menu</strong>-paketet.

  <p>Menupaketet inspirerades av install-fvwm2-menu-programmet från det
  gamla fvwm2-paketet.
  Menu försöker dock tillhandahålla ett mer generellt gränssnitt för att
  skapa menyer.
  Med update-menuskommandot från detta paket behöver inga paket någonsin
  ändras för nya X-fönsterhanterare, och det tillhandahåller ett samlat
  gränssnitt för både text- och X-baserade program.

<doctable>
  <authors "Joost Witteveen, Joey Hess, Christian Schwarz">
  <maintainer "Joost Witteveen">
  <status>
  klar
  </status>
  <availability>
  <inpackage "menu">
  <a href="packaging-manuals/menu.html/">HTML på nätet</a>
  </availability>
</doctable>
</div>

<hr>

<document "De inre delarna av Debian Installer" "d-i-internals">

<div class="centerblock">
<p>
	Detta dokument är menat att göra Debian Installer mer tillgänglig för nya
	utvecklare och som en central plats för att dokumentera teknisk information.
</p>

<doctable>
	<authors "Franz Pop">
	<maintainer "Debian Installer team">
	<status>
	klar
	</status>
	<availability>
	<p><a href="https://d-i.debian.org/doc/internals/">HTML på nätet</a></p>
	<p><a
	href="https://salsa.debian.org/installer-team/debian-installer/tree/master/doc/devel/internals">\
	DocBook XML-källkod på nätet</a></p>
	</availability>
</doctable>
</div>


<document "dbconfig-common dokumentation" "dbconfig-common">

<div class="centerblock">
<p>
	Detta dokument är menat för paketansvariga som underhåller paket som
	kräver en fungerande databas. Istället för att implementera den nödvändiga
	logiken själva så kan dom bero på dbconfig-common för att ställa de
	relevanta frågorna under installation, uppgradering, omkonfiguration och
	avinstallation för dem och skapa och fylla databasen.

<doctable>
  <authors "Sean Finney and Paul Gevers">
  <maintainer "Paul Gevers">
  <status>
  ready
  </status>
  <availability>
  <inpackage "dbconfig-common">
  <inddpvcs-dbconfig-common>
  Utöver detta så finns även ett <a href="/doc/manuals/dbconfig-common/dbconfig-common-design.html">designdokument</a> tillgängligt.
  </availability>
</doctable>
</div>

<hr>

<document "dbapp-policy" "dbapp-policy">

<div class="centerblock">
<p>
	Ett förslag för policy för paket som beror på en fungerande databas.

<doctable>
  <authors "Sean Finney">
  <maintainer "Paul Gevers">
  <status>
  draft
  </status>
  <availability>
  <inpackage "dbconfig-common">
  <inddpvcs-dbapp-policy>
  </availability>
</doctable>
</div>

