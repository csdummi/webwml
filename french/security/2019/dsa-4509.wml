#use wml::debian::translation-check translation="b917e690cbacd447496fcc36bb3b22df5d6873b2" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le serveur web HTTPD
Apache.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9517">CVE-2019-9517</a>

<p>Jonathan Looney a signalé qu'un client malveillant pourrait réaliser une
attaque par déni de service (épuisement des « workers » h2) en inondant une
connexion de requêtes et, essentiellement, en ne lisant jamais les réponses
sur la connexion TCP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10081">CVE-2019-10081</a>

<p>Craig Young a signalé que les fonctions PUSH HTTP/2 pourraient conduire
à un écrasement de mémoire dans la réserve de requêtes d'envois, menant à
des plantages.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10082">CVE-2019-10082</a>

<p>Craig Young a signalé que le traitement de session HTTP/2 pourrait être
conçu pour lire la mémoire après libération lors de la fermeture de la
connexion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10092">CVE-2019-10092</a>

<p>Matei <q>Mal</q> Badanoiu a signalé une vulnérabilité limitée de script
intersite dans la page d'erreur de mod_proxy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10097">CVE-2019-10097</a>

<p>Daniel McCarney a signalé que lorsque mod_remoteip était configuré pour
utiliser un serveur mandataire intermédiaire de confiance utilisant le
protocole <q>PROXY</q>, un en-tête PROXY contrefait pour l'occasion
pourrait déclencher un dépassement de tampon de pile ou un déréférencement
de pointeur NULL. Cette vulnérabilité pourrait être seulement déclenchée
par un mandataire de confiance et non par des clients HTTP non fiables. Ce
problème n'affecte pas la version présente dans Stretch.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10098">CVE-2019-10098</a>

<p>Yukitsugu Sasaki a signalé une possible vulnérabilité de redirection
d'ouverture dans le module mod_rewrite.</p></li>

</ul>

<p>Pour la distribution oldstable (Stretch), ces problèmes ont été corrigés
dans la version 2.4.25-3+deb9u8.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 2.4.38-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets apache2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de apache2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/apache2">\
https://security-tracker.debian.org/tracker/apache2</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4509.data"
# $Id: $
