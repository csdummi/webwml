#use wml::debian::translation-check translation="42b6b660e613679ceb4726fb1a5aebaa47b68d96" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs chercheurs ont découvert des vulnérabilités dans la manière
dont la conception des processeurs Intel a implémenté la transmission
spéculative de données chargées dans des structures microarchitecturales
temporaires (tampons). Ce défaut pourrait permettre à un attaquant
contrôlant un processus non privilégié de lire des informations sensibles,
y compris à partir du noyau et de tous les autres processus exécutés sur le
système ou à travers les limites clients/hôtes pour lire la mémoire de
l'hôte.</p>

<p>Voir <a href="https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/mds.html">\
https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/mds.html</a>
pour plus de détails.</p>

<p>Pour résoudre complètement ces vulnérabilités il est également
nécessaire d'installer le microcode du processeur mis à jour. Un paquet du
microcode d'Intel mis à jour (seulement disponible dans Debian non-free)
sera fourni à l'aide d'une annonce de sécurité particulière. Le microcode
du processeur mis à jour peut aussi être disponible dans le cadre d'une
mise à jour du microprogramme système (« BIOS »).</p>

<p>En complément, cette mise à jour fournit un correctif pour une
régression provoquant des blocages dans le pilote du service loopback,
introduite dans la mise à jour vers 4.9.168 dans la dernière version
intermédiaire de Stretch.</p>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 4.9.168-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4444.data"
# $Id: $
