#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service ou à des
autres impacts.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2188">CVE-2016-2188</a>

<p>Ralf Spenneberg de OpenSource Security a signalé que le pilote de
périphérique iowarrior ne validait pas suffisamment les descripteurs USB. Cela
autorise des utilisateurs physiquement présents avec un périphérique USB
spécialement conçu à provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9604">CVE-2016-9604</a>

<p>Il a été découvert que le sous-système de trousseau de clefs permettait à un
processus de régler un trousseau interne spécial comme trousseau de session.
L’impact de sécurité dans cette version du noyau n’est pas connu.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10200">CVE-2016-10200</a>

<p>Baozeng Ding et Andrey Konovalov ont signalé une situation de compétition
dans l’implémentation de L2TP qui pourrait corrompre sa table de sockets liées.
Un utilisateur local pourrait utiliser cela pour provoquer un déni de service
(plantage) ou éventuellement pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2647">CVE-2017-2647</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-6951">CVE-2017-6951</a>

<p>Idl3r a signalé que le sous-système de trousseau de clefs pourrait permettre
à un processus de rechercher les clefs <q>mortes</q>, causant un déréférencement
de pointeur NULL. Un utilisateur local pourrait utiliser cela pour provoquer un
déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2671">CVE-2017-2671</a>

<p>Daniel Jiang a découvert une situation de compétition dans l’implémentation
ping de socket. Un utilisateur local avec accès aux sockets ping pourrait
utiliser cela pour provoquer un déni de service (plantage) ou éventuellement
pour une élévation des privilèges. Cette fonction n’est pas accessible par défaut
aux utilisateurs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5967">CVE-2017-5967</a>

<p>Xing Gao a signalé que le fichier /proc/timer_list affichait des
informations à propos de tous les processus, sans considérer les espaces de
nommage de PID. Si le débogueur timer était activé par un utilisateur privilégié,
cela divulguait les informations contenues dans cet espace de nommage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5970">CVE-2017-5970</a>

<p>Andrey Konovalov a découvert un défaut de déni de service dans le code
réseau IPv4. Il peut être déclenché par un attaquant local ou distant si
l'option IP_RETOPTS d'une socket local UDP ou brut est activée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7184">CVE-2017-7184</a>

<p>Chaitin Security Research Lab a découvert que le sous-système net xfrm
ne validait pas suffisamment les paramètres d’état de rejeu,
permettant un dépassement de tampon basé sur le tas. Cela peut être utilisé par
un utilisateur local avec la capacité CAP_NET_ADMIN pour une élévation des
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7261">CVE-2017-7261</a>

<p>Vladis Dronov et Murray McAllister ont signalé que le pilote vmwgfx ne
validait pas suffisamment les paramètres de rendu de surface. Dans un client de
VMware, cela peut être être utilisé par un utilisateur local pour provoquer un
déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7273">CVE-2017-7273</a>

<p>Benoit Camredon a signalé que le pilote hid-cypress ne validait pas
suffisamment les rapports HID. Cela éventuellement permettait à un utilisateur
physiquement présent avec un périphérique USB spécialement conçu de provoquer un
déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7294">CVE-2017-7294</a>

<p>Li Qiang a signalé que le pilote vmwgfx ne validait pas suffisamment les
paramètres de rendu de surface. Dans un client de VMware, cela peut être être
utilisé par un utilisateur local pour provoquer un déni de service (plantage).</p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7308">CVE-2017-7308</a>

<p>Andrey Konovalov a signalé que l’implémentation de la socket de paquets
(AF_PACKET) ne validait pas suffisamment les paramètres de tampon. Cela peut
être utilisé par un utilisateur local avec la capacité CAP_NET_RAW pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7472">CVE-2017-7472</a>

<p>Eric Biggers a signalé que le sous-système de trousseau de clefs permettait à
un processus léger de créer de nouveaux trousseaux à plusieurs reprises, causant
une fuite de mémoire. Cela peut être utilisé par un utilisateur local pour
provoquer un déni de service (épuisement de mémoire).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7616">CVE-2017-7616</a>

<p>Chris Salls a signalé une fuite d'informations dans l’implémentation pour compatibilité 32 bits
gros-boutiste de set_mempolicy() et mbind(). Cela n’affecte pas
la prise en charge de n’importe quelle architecture gérée dans Debian 7 LTS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7618">CVE-2017-7618</a>

<p>Sabrina Dubroca a signalé que le sous-système de hachage de chiffrement ne
gère pas correctement la soumission de données non alignées à un périphérique
déjà occupé, aboutissant à une récursion infinie. Sur certains systèmes, cela
peut être utilisé par des utilisateurs locaux pour provoquer un déni de service
(plantage).</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 3.2.88-1. Cette version inclut aussi des corrections de bogue de la
version 3.2.88 de l’amont et corrige quelques problèmes anciens de sécurité dans
les sous-systèmes de trousseau, de socket de paquets et de hachage de
chiffrement qui n’ont pas d’identifiants CVE.</p>

<p>For Debian 8 <q>Jessie</q>, la plupart de ces problèmes ont été corrigés dans
la version 3.16.43-1 qui fera partie la prochaine publication intermédiaire.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-922.data"
# $Id: $
