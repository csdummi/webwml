#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans qemu-kvm, une solution
complète de virtualisation pour des hôtes Linux sur du matériel x86 avec des
invités x86 basée sur Qemu (Quick Emulator).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14167">CVE-2017-14167</a>

<p>Une validation incorrecte d’en-têtes multiboot pourrait aboutir à l'exécution de
code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15038">CVE-2017-15038</a>

<p>Lors de l’utilisation de 9pfs, qemu-kvm est vulnérable à un problème de
divulgation d'informations. Il pourrait se produire lors de l’accès à des
attributs étendus d’un fichier dû à une situation de compétition. Cela pourrait
être utilisé pour divulguer le contenu de la mémoire de tas de l’hôte.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.1.2+dfsg-6+deb7u24.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu-kvm.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1128.data"
# $Id: $
