#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité de sécurité a été découverte dans OpenSSL, la boîte à
outils associée à SSL (Secure Socket Layer).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-3735">CVE-2017-3735</a>

<p>OpenSSL est prédisposé à une lecture excessive d’un octet lors de l’analyse
d’une extension IPAddressFamily mal formée dans un certificat X.509.</p>

<p>Plus de détails sont disponibles dans l’annonce de l’amont :
<a href="https://www.openssl.org/news/secadv/20170828.txt">https://www.openssl.org/news/secadv/20170828.txt</a></p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.0.1t-1+deb7u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssl.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1157.data"
# $Id: $
