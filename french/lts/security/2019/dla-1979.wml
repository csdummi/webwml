#use wml::debian::translation-check translation="3918b0d45e0d283f86118204cd22dda8427cf069" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été identifiées dans le code VNC de iTALC, un
logiciel de gestion d’enseignement. Toutes les vulnérabilités référencées
ci-dessous sont des problèmes qui ont été signalés à l’origine pour le paquet
source libvncserver. Le paquet source italc dans Debian fournit une version
personnalisée de libvncserver, par conséquent les corrections de sécurité de
libvncserver nécessitent un portage.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-6051">CVE-2014-6051</a>

<p>Un dépassement d’entier dans la fonction MallocFrameBuffer dans vncviewer.c
dans LibVNCServer permettait à des serveurs VNC distants de provoquer un déni de
service (plantage) et éventuellement l’exécution de code arbitraire à
l'aide d'une annonce pour une taille d’écran très grande qui déclenchait un
dépassement de tampon basé sur le tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-6052">CVE-2014-6052</a>

<p>La fonction HandleRFBServerMessage dans libvncclient/rfbproto.c dans
LibVNCServer ne vérifiait pas certaines valeurs malloc de renvoi, qui permettait
à des serveurs VNC distants de provoquer un déni de service (plantage
d'application) ou éventuellement d’exécuter du code arbitraire en indiquant une
taille d’écran très grande dans un message (1) FramebufferUpdate,
(2) ResizeFrameBuffer, ou (3) PalmVNCReSizeFrameBuffer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-6053">CVE-2014-6053</a>

<p>La fonction rfbProcessClientNormalMessage dans libvncserver/rfbserver.c dans
LibVNCServer ne gérait pas correctement les tentatives d’envoi d’un grand
montant de données ClientCutText, qui permettait à des attaquants distants
de provoquer un déni de service (consommation de mémoire ou plantage du démon) à
l'aide d'un message contrefait qui était traité en utilisant un seul malloc
non vérifié.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-6054">CVE-2014-6054</a>

<p>La fonction rfbProcessClientNormalMessage dans libvncserver/rfbserver.c dans
LibVNCServer permettait à des attaquants distants de provoquer un déni de
service (erreur de division par zéro et plantage du serveur) à l'aide d'une
valeur zéro dans le facteur d’échelle dans un message (1) PalmVNCSetScaleFactor
ou (2) SetScale.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-6055">CVE-2014-6055</a>

<p>Plusieurs dépassements de pile dans la fonction de transfert de fichier dans
rfbserver.c dans LibVNCServer permettaient à des utilisateurs distants
authentifiés de provoquer un déni de service (plantage) et éventuellement
d’exécuter du code arbitraire à l'aide de (1) un long fichier ou (2) un nom de
répertoire ou (3) l’attribut FileTime dans un message rfbFileTransferOffer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9941">CVE-2016-9941</a>

<p>Un dépassement de tampon basé sur le tas dans rfbproto.c dans LibVNCClient
dans LibVNCServer permettait à un serveur distant de provoquer un déni de
service (plantage d'application) ou éventuellement d’exécuter du code arbitraire
à l'aide d'un message contrefait FramebufferUpdate contenant un sous-rectangle
en dehors de l’aire de dessin du client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9942">CVE-2016-9942</a>

<p>Un dépassement de tampon basé sur le tas dans ultra.c dans LibVNCClient dans
LibVNCServer permettait à un serveur distant de provoquer un déni de service
(plantage d'application) ou éventuellement d’exécuter du code arbitraire à
l'aide d'un message contrefait FramebufferUpdate avec une tuile de type Ultra,
de telle façon que la longueur de charge LZO décompressée excédait les
dimensions indiquées de tuile.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6307">CVE-2018-6307</a>

<p>LibVNC contenait une vulnérabilité d’utilisation de tas après libération dans
le code de serveur de l’extension de transfert de fichier qui pourrait aboutir à
une exécution de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7225">CVE-2018-7225</a>

<p>Un problème a été découvert dans LibVNCServer.
rfbProcessClientNormalMessage() dans rfbserver.c ne nettoyait pas msg.cct.length,
conduisant à un accès à des données non initialisées et éventuellement sensibles
ou éventuellement avoir un autre impact non précisé (par exemple, un dépassement
d'entier) à l’aide de paquets VNC contrefaits pour l'occasion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15126">CVE-2018-15126</a>

<p>LibVNC contenait une vulnérabilité d’utilisation de tas après libération dans
le code de serveur de l’extension de transfert de fichier qui pouvait aboutir
à une exécution de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15127">CVE-2018-15127</a>

<p>LibVNC contenait une vulnérabilité d’écriture de tas hors limites dans
le code de serveur de l’extension de transfert de fichier qui pouvait aboutir
à une exécution de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20749">CVE-2018-20749</a>

<p>LibVNC contenait une vulnérabilité d’écriture de tas hors limites dans
libvncserver/rfbserver.c. Le correctif pour
<a href="https://security-tracker.debian.org/tracker/CVE-2018-15127">CVE-2018-15127</a>
était incomplet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20750">CVE-2018-20750</a>

<p>LibVNC contenait une vulnérabilité d’écriture de tas hors limites dans
libvncserver/rfbserver.c. Le correctif pour
<a href="https://security-tracker.debian.org/tracker/CVE-2018-15127">CVE-2018-15127</a>
était incomplet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20019">CVE-2018-20019</a>

<p>LibVNC contenait plusieurs vulnérabilités d’écriture de tas hors limites dans
le code de VNC client qui pourrait aboutir à l’exécution de code à distance</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20748">CVE-2018-20748</a>

<p>LibVNC contenait plusieurs vulnérabilités d’écriture de tas hors limites dans
libvncclient/rfbproto.c. Le correctif pour
<a href="https://security-tracker.debian.org/tracker/CVE-2018-20019">CVE-2018-20019</a>
était incomplet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20020">CVE-2018-20020</a>

<p>LibVNC contenait une vulnérabilité d’écriture de tas hors limites dans une
structure dans le code de VNC client qui pourrait aboutir à l’exécution de code
à distance</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20021">CVE-2018-20021</a>

<p>LibVNC contenait une CWE-835 : vulnérabilité de boucle infinie dans le code
de VNC client. Cette vulnérabilité permet à un attaquant de consommer un montant
excessif de ressources telles que de CPU et RAM</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20022">CVE-2018-20022</a>

<p>LibVNC contenait plusieurs faiblesses CWE-665 : vulnérabilité d’initialisation
dans le code de VNC client qui permettaient être des attaquants de lire la
mémoire de pile et pourrait être abusé pour divulguer des informations.
Combinées avec une autre vulnérabilité, cela pourrait être utilisé pour
divulguer la disposition de mémoire de pile et dans un contournement ASLR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20023">CVE-2018-20023</a>

<p>LibVNC contenait une CWE-665 : vulnérabilité d’initialisation incorrecte dans le
code de VNC client Repeater, qui permettait à un attaquant de lire la mémoire de
pile et pourrait être abusé pour divulguer des informations. Combinée avec une
autre vulnérabilité, cela pourrait être utilisé pour divulguer la disposition de
mémoire de pile et dans un contournement ASLR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20024">CVE-2018-20024</a>

<p>LibVNC contenait un déréférencement de pointeur NULL dans le code de VNC
client qui pourrait aboutir à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15681">CVE-2019-15681</a>

<p>LibVNC contenait une fuite de mémoire (CWE-655) dans le code de VNC serveur
qui permettait à un attaquant de lire la mémoire de pile et pourrait être abusé
pour divulguer des informations. Combinée avec une autre vulnérabilité, cela
pourrait être utilisé pour divulguer la mémoire de pile et dans un contournement
ASLR. Cette attaque apparaissait être exploitable à l’aide de la connexion au
réseau.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1:2.0.2+dfsg1-2+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets italc.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1979.data"
# $Id: $
