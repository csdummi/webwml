#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000805">CVE-2018-1000805</a>

<p>Correctif pour empêcher des clients malveillants de tromper le serveur
Paramiko en lui faisant croire qu’un client non authentifié est authentifié.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7750">CVE-2018-7750</a>

<p>Correctif pour que la vérification soit terminée avant de traiter d’autres
requêtes. Un client SSH personnalisé peut simplement omettre cette étape
d’authentification.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.15.1-1+deb8u1.</p>.
<p>Nous vous recommandons de mettre à jour vos paquets paramiko.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1556.data"
# $Id: $
