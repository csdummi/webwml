#use wml::debian::translation-check translation="2913230d58de12a2d0daeab2fd1f532a65fa5c2a" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 9.12</define-tag>
<define-tag release_date>2020-02-08</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>9</define-tag>
<define-tag codename>Stretch</define-tag>
<define-tag revision>9.12</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Le projet Debian a l'honneur d'annoncer la douzième mise à jour de sa
distribution oldstable Debian <release> (non de code <q><codename></q>). 
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version oldstable. Les annonces
de sécurité ont déjà été publiées séparément et sont simplement référencées dans
ce document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version
de Debian <release> mais seulement une mise à jour de certains des paquets
qu'elle contient. Il n'est pas nécessaire de jeter les anciens médias de
la version <codename>. Après installation, les paquets peuvent être mis à
niveau vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette
mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP
de Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrections de bogues divers</h2>

<p>Cette mise à jour de la version oldstable apporte quelques corrections
importantes aux paquets suivants :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction base-files "Mise à jour pour cette version">
<correction cargo "Nouvelle version amont, pour la prise en charge des rétroportages de Firefox ESR ; correction de bootstrap pour armhf">
<correction clamav "Nouvelle version amont ; correction d'un problème de déni de service [CVE-2019-15961] ; retrait de l'option ScanOnAccess, remplacée par clamonacc">
<correction cups "Correction de la validation de la langue par défaut dans ippSetValuetag [CVE-2019-2228]">
<correction debian-installer "Reconstruction avec oldstable-proposed-updates ; réglage gfxpayload=keep aussi dans les sous-menus pour corriger les fontes illisibles dans les affichages hidpi des images netboot amorcées avec EFI ; mise à jour de unstable à Stretch de USE_UDEBS_FROM par défaut pour aider les utilisateurs à réaliser des constructions locales">
<correction debian-installer-netboot-images "Reconstruction avec stretch-proposed-updates">
<correction debian-security-support "Mise à jour de l’état de la prise en charge du suivi de sécurité de divers paquets">
<correction dehydrated "Nouvelle version amont ; utilisation de l'API ACMEv2 par défaut">
<correction dispmua "Nouvelle version amont compatible avec Thunderbird 68">
<correction dpdk "Nouvelle version amont stable ; correction d'une régression de vhost introduite par la correction pour le CVE-2019-14818">
<correction fence-agents "Correction d'un retrait incomplet de fence_amt_ws">
<correction fig2dev "Chaînes de texte Fig v2 se terminant par de multiples ^A permises [CVE-2019-19555]">
<correction flightcrew "Corrections de sécurité [CVE-2019-13032 CVE-2019-13241]">
<correction freetype "Gestion correcte des deltas dans les fontes GX de TrueType, corrigeant le rendu des fontes variables lissées dans Chromium et Firefox">
<correction glib2.0 "Assurance que les clients libdbus peuvent s'identifier avec un GDBusServer comme celui dans ibus">
<correction gnustep-base "Correction d'une vulnérabilité d'amplification d'UDP">
<correction italc "Corrections de sécurité [CVE-2018-15126 CVE-2018-15127 CVE-2018-20019 CVE-2018-20020 CVE-2018-20021 CVE-2018-20022 CVE-2018-20023 CVE-2018-20024 CVE-2018-20748 CVE-2018-20749 CVE-2018-20750 CVE-2018-6307 CVE-2018-7225 CVE-2019-15681]">
<correction libdate-holidays-de-perl "Marquage de la journée internationale de l'enfance (20 septembre) comme fériée à partir de 2019 en Thuringe">
<correction libdatetime-timezone-perl "Mise à jour des données incluses">
<correction libidn "Correction d'une vulnérabilité de déni de service dans la gestion de Punycode [CVE-2017-14062]">
<correction libjaxen-java "Correction d'échec de construction en permettant des échecs de test">
<correction libofx "Correction d'un problème de déréférencement de pointeur NULL  [CVE-2019-9656]">
<correction libole-storage-lite-perl "Correction de l'interprétation des années à partir de 2020">
<correction libparse-win32registry-perl "Correction de l'interprétation des années à partir de 2020">
<correction libperl4-corelibs-perl "Correction de l'interprétation des années à partir de 2020">
<correction libpst "Correction de la détection de get_current_dir_name et de troncature de retour">
<correction libsixel "Correction de plusieurs problèmes de sécurité [CVE-2018-19756 CVE-2018-19757 CVE-2018-19759 CVE-2018-19761 CVE-2018-19762 CVE-2018-19763 CVE-2019-3573 CVE-2019-3574]">
<correction libsolv "Correction de dépassement de tas [CVE-2019-20387]">
<correction libtest-mocktime-perl "Correction de l'interprétation des années à partir de 2020">
<correction libtimedate-perl "Correction de l'interprétation des années à partir de 2020">
<correction libvncserver "RFBserver : pas de fuite de mémoire de pile vers un attaquant distant [CVE-2019-15681] ; gel durant la clôture de connexion et erreur de segmentation résolus dans les serveurs VNC multifils ; correction d'un problème de connexion aux serveurs VMWare ; correction de plantage de x11vnc lors de la connexion de vncviewer">
<correction libxslt "Correction de pointeur bancal dans xsltCopyText [CVE-2019-18197]">
<correction limnoria "Correction de divulgation d'informations et d'exécution potentielle de code à distance dans le greffon Math [CVE-2019-19010]">
<correction linux "Nouvelle version amont stable">
<correction linux-latest "Mise à jour de l'ABI du noyau Linux 4.9.0-12">
<correction llvm-toolchain-7 "Désactivation de l'éditeur de liens « gold » de s390x ; bootstrap avec -fno-addrsig, la version de binutils de Stretch ne fonctionne pas sur mips64el">
<correction mariadb-10.1 "Nouvelle version amont stable [CVE-2019-2974 CVE-2020-2574]">
<correction monit "Implémentation de valeur de cookie CSRF indépendante de la position">
<correction node-fstream "Lien remplacé s'il est sur le chemin d'un fichier [CVE-2019-13173]">
<correction node-mixin-deep "Correction de pollution de prototype [CVE-2018-3719 CVE-2019-10746]">
<correction nodejs-mozilla "Nouveau paquet pour prendre en charge les rétroportages de Firefox ESR">
<correction nvidia-graphics-drivers-legacy-340xx "Nouvelle version amont stable">
<correction nyancat "Reconstruction dans un environnement propre pour ajouter un fichier unit de systemd pour nyancat-server">
<correction openjpeg2 "Correction de dépassement de tas [CVE-2018-21010], de dépassement d'entier [CVE-2018-20847] et de division par zéro [CVE-2016-9112]">
<correction perl "Correction de l'interprétation des années à partir de 2020">
<correction php-horde "Correction d'un problème de script intersite stocké dans Cloud Block de Horde [CVE-2019-12095]">
<correction postfix "Nouvelle version stable amont ; contournement des mauvaises performances de la boucle locale TCP">
<correction postgresql-9.6 "Nouvelle version amont">
<correction proftpd-dfsg "Correction de déréférencement de pointeur NULL dans les vérifications de CRL [CVE-2019-19269]">
<correction pykaraoke "Correction du chemin vers les fontes">
<correction python-acme "Passage au protocole POST-as-GET">
<correction python-cryptography "Correction d'échec de la suite de tests lors de la construction avec les nouvelles versions d'OpenSSL">
<correction python-flask-rdf "Correction de dépendances manquantes dans python3-flask-rdf">
<correction python-pgmagick "Gestion de la détection de version des mises à jour de sécurité de graphicsmagick qui s'identifient comme version 1.4">
<correction python-werkzeug "Assurance que les conteneurs Docker ont des codes PIN de débogage uniques [CVE-2019-14806]">
<correction ros-ros-comm "Correction d'un problème de dépassement de tampon [CVE-2019-13566] ; correction de dépassement d'entier [CVE-2019-13445]">
<correction ruby-encryptor "Échecs des tests ignorés, corrigeant les échecs de construction">
<correction rust-cbindgen "Nouvelle version amont pour prendre en charge les rétroportages de Firefox ESR">
<correction rustc "Nouvelle version amont pour prendre en charge les rétroportages de Firefox ESR">
<correction safe-rm "Installation évitée (et donc interrompue) dans les environnements avec /usr fusionné">
<correction sorl-thumbnail "Contournement d'une exception de pgmagick">
<correction sssd "sysdb : entrées de filtre de recherche nettoyées [CVE-2017-12173]">
<correction tigervnc "Mises à jour de sécurité [CVE-2019-15691 CVE-2019-15692 CVE-2019-15693 CVE-2019-15694 CVE-2019-15695]">
<correction tightvnc "Corrections de sécurité [CVE-2014-6053 CVE-2018-20021 CVE-2018-20022 CVE-2018-20748 CVE-2018-7225 CVE-2019-8287 CVE-2019-15678 CVE-2019-15679 CVE-2019-15680 CVE-2019-15681]">
<correction tmpreaper "Ajout de <q>--protect '/tmp/systemd-private*/*'</q> aux tâches de cron pour éviter de casser les services de systemd configurés avec PrivateTmp=true">
<correction tzdata "Nouvelle version amont">
<correction ublock-origin "Nouvelle version amont, compatible avec Firefox ESR68">
<correction unhide "Correction d'épuisement de pile">
<correction x2goclient "Retrait de ~/, ~user{,/}, ${HOME}{,/} et $HOME{,/} des chemins de destination en mode SCP ; corrections de régression avec les dernières versions de libssh avec application des correctifs pour le CVE-2019-14889">
<correction xml-security-c "Correction de <q>la vérification de DSA plante OpenSSL avec des combinaisons non valables de contenus de clé</q>">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
oldstable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2019 4474 firefox-esr>
<dsa 2019 4479 firefox-esr>
<dsa 2019 4509 apache2>
<dsa 2019 4509 subversion>
<dsa 2019 4511 nghttp2>
<dsa 2019 4516 firefox-esr>
<dsa 2019 4517 exim4>
<dsa 2019 4518 ghostscript>
<dsa 2019 4519 libreoffice>
<dsa 2019 4522 faad2>
<dsa 2019 4523 thunderbird>
<dsa 2019 4525 ibus>
<dsa 2019 4526 opendmarc>
<dsa 2019 4528 bird>
<dsa 2019 4529 php7.0>
<dsa 2019 4530 expat>
<dsa 2019 4531 linux>
<dsa 2019 4532 spip>
<dsa 2019 4535 e2fsprogs>
<dsa 2019 4537 file-roller>
<dsa XXXX 4539 openssl>
<dsa 2019 4540 openssl1.0>
<dsa 2019 4541 libapreq2>
<dsa 2019 4542 jackson-databind>
<dsa 2019 4543 sudo>
<dsa 2019 4545 mediawiki>
<dsa 2019 4547 tcpdump>
<dsa 2019 4548 openjdk-8>
<dsa XXXX 4549 firefox-esr>
<dsa 2019 4550 file>
<dsa 2019 4552 php7.0>
<dsa 2019 4554 ruby-loofah>
<dsa 2019 4555 pam-python>
<dsa 2019 4557 libarchive>
<dsa 2019 4559 proftpd-dfsg>
<dsa 2019 4560 simplesamlphp>
<dsa 2019 4564 linux>
<dsa 2019 4565 intel-microcode>
<dsa 2019 4567 dpdk>
<dsa 2019 4568 postgresql-common>
<dsa 2019 4569 ghostscript>
<dsa 2019 4571 thunderbird>
<dsa 2019 4573 symfony>
<dsa 2019 4574 redmine>
<dsa 2019 4576 php-imagick>
<dsa 2019 4578 libvpx>
<dsa 2019 4580 firefox-esr>
<dsa 2019 4581 git>
<dsa 2019 4582 davical>
<dsa 2019 4584 spamassassin>
<dsa 2019 4585 thunderbird>
<dsa 2019 4587 ruby2.3>
<dsa 2019 4588 python-ecdsa>
<dsa 2019 4589 debian-edu-config>
<dsa 2019 4590 cyrus-imapd>
<dsa 2019 4591 cyrus-sasl2>
<dsa 2019 4592 mediawiki>
<dsa 2019 4593 freeimage>
<dsa 2019 4594 openssl1.0>
<dsa 2019 4595 debian-lan-config>
<dsa 2019 4596 tomcat8>
<dsa XXXX 4596 tomcat-native>
<dsa 2020 4597 netty>
<dsa 2020 4598 python-django>
<dsa 2020 4600 firefox-esr>
<dsa 2020 4601 ldm>
<dsa 2020 4602 xen>
<dsa 2020 4603 thunderbird>
<dsa 2020 4604 cacti>
<dsa 2020 4607 openconnect>
<dsa 2020 4609 python-apt>
<dsa XXXX 4611 opensmtpd>
<dsa 2020 4612 prosody-modules>
<dsa 2020 4614 sudo>
<dsa 2020 4615 spamassassin>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction firetray "Incompatible avec les versions actuelles de Thunderbird">
<correction koji "Problèmes de sécurité">
<correction python-lamson "Cassé par des modifications dans python-daemon">
<correction radare2 "Problèmes de sécurité ; l'amont n'offre pas de prise en charge stable">
<correction ruby-simple-form "Inutilisé ; problèmes de sécurité">
<correction trafficserver "Non pris en charge">

</table>

<h2>Installateur Debian</h2>
<p>L'installateur a été mis à jour pour inclure les correctifs incorporés
dans cette version de oldstable.</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Adresse de l'actuelle distribution oldstable :</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable/">
</div>

<p>
Mises à jour proposées à la distribution oldstable :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>
Informations sur la distribution oldstable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres
qui offrent volontairement leur temps et leurs efforts pour produire le
système d'exploitation complètement libre Debian.</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de
publication de la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>
