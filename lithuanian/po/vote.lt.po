#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2009-03-13 16:11+0300\n"
"Last-Translator: Aleksandr Charkov <alcharkov@gmail.com>\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "Data"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "Laiko riba"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr ""

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "Paskyrimai"

#: ../../english/template/debian/votebar.wml:25
#, fuzzy
msgid "Withdrawals"
msgstr "Atšaukta"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "Debatai"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "Platformos"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "Siūlytojas"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "Siūlytojo Pasiūlymas A"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "Siūlytojo Pasiūlymas B"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "Siūlytojo Pasiūlymas C"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "Siūlytojo Pasiūlymas D"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "Siūlytojo Pasiūlymas E"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "Siūlytojo Pasiūlymas F"

#: ../../english/template/debian/votebar.wml:55
#, fuzzy
msgid "Proposal G Proposer"
msgstr "Siūlytojo Pasiūlymas A"

#: ../../english/template/debian/votebar.wml:58
#, fuzzy
msgid "Proposal H Proposer"
msgstr "Siūlytojo Pasiūlymas A"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "Palaikantieji"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "Palaikantys Pasiūlymą A"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "Palaikantys Pasiūlymą B"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "Palaikantys Pasiūlymą C"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "Palaikantys Pasiūlymą D"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "Palaikantys Pasiūlymą E"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "Palaikantys Pasiūlymą F"

#: ../../english/template/debian/votebar.wml:82
#, fuzzy
msgid "Proposal G Seconds"
msgstr "Palaikantys Pasiūlymą A"

#: ../../english/template/debian/votebar.wml:85
#, fuzzy
msgid "Proposal H Seconds"
msgstr "Palaikantys Pasiūlymą A"

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "Opozicija"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "Tekstas"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "Pasiūlymas A"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "Pasiūlymas B"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "Pasiūlymas C"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "Pasiūlymas D"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "Pasiūlymas E"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "Pasiūlymas F"

#: ../../english/template/debian/votebar.wml:112
#, fuzzy
msgid "Proposal G"
msgstr "Pasiūlymas A"

#: ../../english/template/debian/votebar.wml:115
#, fuzzy
msgid "Proposal H"
msgstr "Pasiūlymas A"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "Pasirinkimai"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "Pataisos Siūlytojas"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "Palaikantys pataisą"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "Pataisos Tekstas"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "Pataisos Siūlytojas A"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "Pataisos palaikytojai A"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "Pataisos Tekstas A"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "Pataisos Siūlytojas B"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "Pataisos palaikytojai B"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "Pataisos Tekstas B"

#: ../../english/template/debian/votebar.wml:148
#, fuzzy
msgid "Amendment Proposer C"
msgstr "Pataisos Siūlytojas A"

#: ../../english/template/debian/votebar.wml:151
#, fuzzy
msgid "Amendment Seconds C"
msgstr "Pataisos palaikytojai A"

#: ../../english/template/debian/votebar.wml:154
#, fuzzy
msgid "Amendment Text C"
msgstr "Pataisos Tekstas A"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "Pataisos"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "Posėdžiai"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "Daugumos reikalavimas"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "Duomenys ir Statistika"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "Kvorumas"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "Minimalios diskusijos"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "Balsavimo kortelė"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "Forumas"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "Rezultatas"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "Laukia&nbsp;rėmėjų"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "Vyksta&nbsp;diskusijos"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "Atviras&nbsp;balsavimas"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "Nuspręsta"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "Atšaukta"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "Kita"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "Pradinis&nbsp;balsavimo&nbsp;puslapis"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "Kaip"

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Pateikti&nbsp;siūlymą"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Keisti&nbsp;siūlymą"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Sekti&nbsp;siūlymą"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Skaityti&nbsp;rezultatą"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "Balsuoti"
