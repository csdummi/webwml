<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that python-ecdsa, a cryptographic signature library
for Python, incorrectly handled certain signatures. A remote attacker
could use this issue to cause python-ecdsa to either not warn about
incorrect signatures, or generate exceptions resulting in a
denial-of-service.</p>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 0.13-2+deb9u1.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 0.13-3+deb10u1.</p>

<p>We recommend that you upgrade your python-ecdsa packages.</p>

<p>For the detailed security status of python-ecdsa please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python-ecdsa">\
https://security-tracker.debian.org/tracker/python-ecdsa</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4588.data"
# $Id: $
