<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The tcprewrite program, part of the tcpreplay suite, does not check the
size of the frames it processes. Huge frames may trigger a segmentation
fault, and such frames occur when caputuring packets on interfaces with
an MTU of or close to 65536. For example, the loopback interface lo of
the Linux kernel has such a value.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.4.3-2+wheezy2.</p>

<p>We recommend that you upgrade your tcpreplay packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-544.data"
# $Id: $
