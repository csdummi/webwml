<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the Sleuth Kit (TSK) through version 4.6.4 is
affected by a buffer over-read vulnerability. The tsk_getu16 call in
hfs_dir_open_meta_cb (tsk/fs/hfs_dent.c) does not properly check
boundaries. This vulnerability might be leveraged by remote attackers
using crafted filesystem images to cause denial of service or any other
unspecified behavior.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
4.1.3-4+deb8u1.</p>

<p>We recommend that you upgrade your sleuthkit packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1610.data"
# $Id: $
