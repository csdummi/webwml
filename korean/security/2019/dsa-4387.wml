#use wml::debian::translation-check translation="19fdc288616ee3bfe6ee122b16cd10940121ffb2" maintainer="Sebul" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.
<define-tag description>보안 업데이트</define-tag>
<define-tag moreinfo>
<p>F-Secure Corporation의 Harry Sintonen은 SSH 프로토콜 제품군의 구현인 OpenSSH에서 여러 가지 취약점을 발견했습니다.
모든 취약점은 SCP 프로토콜을 구현하는 scp 클라이언트에서 찾았습니다.
</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20685">CVE-2018-20685</a>

    <p>Due to improper directory name validation, the scp client allows servers to
    modify permissions of the target directory by using empty or dot directory
    name.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6109">CVE-2019-6109</a>

    <p>Due to missing character encoding in the progress display, the object name
    can be used to manipulate the client output, for example to employ ANSI
    codes to hide additional files being transferred.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6111">CVE-2019-6111</a>

    <p>Due to scp client insufficient input validation in path names sent by
    server, a malicious server can do arbitrary file overwrites in target
    directory. If the recursive (-r) option is provided, the server can also
    manipulate subdirectories as well.</p>

    <p>The check added in this version can lead to regression if the client and
    the server have differences in wildcard expansion rules. If the server is
    trusted for that purpose, the check can be disabled with a new -T option to
    the scp client.</p></li>

</ul>

<p>안정 배포(stretch)에서, 이 문제를 버전 1:7.4p1-10+deb9u5에서 고쳤습니다.
</p>

<p>openssh 패키지를 업그레이드 하는 게 좋습니다.</p>

<p>openssh의 자세한 보안 상태는 보안 추적 페이지 참조:
<a href="https://security-tracker.debian.org/tracker/openssh">\
https://security-tracker.debian.org/tracker/openssh</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4387.data"
